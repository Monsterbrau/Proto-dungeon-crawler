/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/game.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/game.js":
/*!************************!*\
  !*** ./src/js/game.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _player__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./player */ \"./src/js/player.js\");\n/* harmony import */ var _mob__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mob */ \"./src/js/mob.js\");\n/* harmony import */ var _level__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./level */ \"./src/js/level.js\");\n\n\n\n\n\n\nlet player;\nlet game = [];\nlet ennemy = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"]();\n\n\nlet stage = 5;\nlet forest = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\nlet hard = 1;\nlet land = \"\";\n\nfunction start() {\n  console.log(\"to finish the stage : gain 10 level !\");\n  document.removeEventListener('keydown', start);\n  land = \"wood\"\n  game = forest.generate();\n  player = new _player__WEBPACK_IMPORTED_MODULE_0__[\"Player\"](\"Monsterbrau\", 0, 0, 7, 65, 2, 5, 5, 0, 0, 0, [], 55, stage);\n  game[player.x][player.y];\n  console.log(game);\n  player.toDraw();\n  console.log(\"use z,q,s,d, keys in your keyboard to move into the dungeon\");\n}\n\nfunction toMove(event) {\n  gainLevel();\n  if (event.keyCode === 90) {\n    player.moveUp();\n    // board.innerHTML = \"\";\n    // drawGrid(game);\n  } else if (event.keyCode === 83) {\n    player.moveDown();\n    // board.innerHTML = \"\";\n    // drawGrid(game);\n  } else if (event.keyCode === 81) {\n    player.moveLeft();\n    // board.innerHTML = \"\";\n    // drawGrid(game);\n  } else if (event.keyCode === 68) {\n    player.moveRight();\n    // board.innerHTML = \"\";\n    // drawGrid(game);\n  }\n\n  console.log(game[player.x][player.y]);\n\n  if ((game[player.x][player.y].randomEvent) === 'treasure') {\n    treasure();\n  }\n\n  if (game[player.x][player.y].randomEvent === \"ennemy\") {\n    console.log(`you have encounter an ennemy !`);\n    document.removeEventListener('keydown', toMove);\n    console.log('press enter to begin the fight');\n    document.addEventListener('keydown', toFight);\n  }\n\n  if (game[player.x][player.y].randomEvent === \"enygma\") {\n    enygma();\n  }\n\n  if (game[player.x][player.y].randomEvent === \"trap\") {\n    trap();\n  }\n\n  if (game[player.x][player.y].randomEvent === 'trapped-treasure') {\n    trapppedTreasure();\n  }\n\n  if (game[player.x][player.y].randomEvent === 'boss') {\n    document.removeEventListener('keydown', toMove);\n    console.log('press enter to begin the fight');\n    document.addEventListener('keydown', toFight);\n  }\n\n  if (game[player.x][player.y].randomEvent === 'armory') {\n    armory();\n  }\n  if (game[player.x][player.y].randomEvent === 'sword master') {\n    swordMaster();\n  }\n  if (game[player.x][player.y].randomEvent === 'rogue master') {\n    rogueMaster();\n  }\n  if (game[player.x][player.y].randomEvent === 'thaumaturge') {\n    thaumaturge();\n  }\n}\n\nfunction mobGeneration() {\n  let mobName = nameMobGenerator();\n  let percent = Math.floor(Math.random() * hard + 45);\n  let num = Math.floor(Math.random() * hard * 2);\n  if (num === 0) {\n    num += hard;\n  }\n  let mob = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"](mobName, num, percent, num, num, num);\n  return mob;\n}\n\nfunction bossGeneration() {\n  let mobName = nameBossGenerator(land);\n  let percent = Math.floor(Math.random() * hard + 65);\n  let num = Math.floor(Math.random() * hard * 5);\n  if (num === 0) {\n    num += 5;\n  }\n  let boss = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"](mobName, num, percent, num, num, num);\n  boss.boss = true;\n  return boss;\n}\n\nfunction firstStrike() {\n  console.log(`you have first strike !`);\n  let dd = 100;\n  let attack = Math.floor(Math.random() * dd);\n  if (attack <= player.force) {\n    console.log(`attack success with ${attack} !`);\n    let hit = player.totalAttack() - ennemy.armor;\n    if (hit <= 0) {\n      hit = 1\n    }\n    ennemy.pv -= hit;\n    dd -= 10;\n    console.log(`you have made ${hit} dommages !`);\n    ennemy.mobDeath();\n  } else {\n    console.log('attack fail !');\n    let dd = 100;\n    let attack = Math.floor(Math.random() * dd);\n    if (attack <= ennemy.force) {\n      console.log(`${ennemy.name} attack success with ${attack} !`);\n      let hit = ennemy.dg - player.totalArmor();\n      if (hit <= 0) {\n        hit = 1\n      }\n      player.pv -= hit;\n      dd -= 10\n      console.log(`you have take ${hit} dommages !`);\n      player.death();\n    } else {\n      console.log(`${ennemy.name}'s attack fail !`);\n    }\n  };\n\n}\n\nfunction secondStrike() {\n  console.log(`${ennemy.name} first strike !`);\n  let dd = 100;\n  let attack = Math.floor(Math.random() * dd);\n  if (attack <= ennemy.force) {\n    console.log(`${ennemy.name} attack success with ${attack} !`);\n    let hit = ennemy.dg - player.totalArmor();\n    if (hit <= 0) {\n      hit = 1\n    }\n    player.pv -= hit;\n    dd -= 10;\n    console.log(`you have take ${hit} dommages !`);\n    player.death();\n  } else {\n    console.log(`${ennemy.name}'s attack fail !`);\n    let dd = 100;\n    let attack = Math.floor(Math.random() * dd);\n    if (attack <= player.force) {\n      console.log(`attack success with ${attack} !`);\n      let hit = player.totalAttack() - ennemy.armor;\n      if (hit <= 0) {\n        hit = 1\n      }\n      ennemy.pv -= hit;\n      dd -= 10;\n      console.log(`you have made ${hit} dommages !`);\n      ennemy.mobDeath();\n    } else {\n      console.log('attack fail !');\n    }\n  }\n}\n\nfunction toFight(event) {\n  if (event.keyCode === 13) {\n    if (game[player.x][player.y].randomEvent === 'boss') {\n      ennemy = bossGeneration();\n    } else { ennemy = mobGeneration(hard); };\n    console.log(ennemy);\n    while (player.pv > 0 && ennemy.pv > 0) {\n      if (player.init >= ennemy.init) {\n        firstStrike();\n      } else {\n        secondStrike();\n      }\n    }\n    if (ennemy.boss) {\n      hard++;\n      player.gold += moneyGeneration();\n      player.level++;\n    }\n    player.xp += (hard + 3);\n    console.log(`player xp : ${player.xp}`);\n    player.gold += moneyGeneration();\n    gainLevel();\n    console.log(`player pv : ${player.pv}`);\n    console.log(`player gold : ${player.gold}`);\n    console.log(`player level : ${player.level}`);\n    console.log(`difficulty : ${hard}`);\n\n    game[player.x][player.y].randomEvent = 'empty';\n    document.removeEventListener(\"keydown\", toFight);\n    document.addEventListener('keydown', toMove);\n  }\n}\n\nfunction gainLevel() {\n\n  if (player.xp >= 5) {\n    player.xp -= 5;\n    player.level++;\n    player.force += 2;\n    player.init++;\n    player.pv += hard;\n    console.log(`you are level ${player.level}.`);\n    console.log(player);\n  }\n  if (player.level === 10) {\n    console.log(`you have win the ${land} stage !`);\n    document.removeEventListener(\"keydown\", toFight);\n    game = \"\";\n    let mountain = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\n    game = mountain.generate();\n    hard += 2;\n    player.level = 0;\n    player.x = 0;\n    player.y = 0;\n    land = \"mountain\";\n  }\n  if (player.level === 20) {\n    console.log(`you have win the ${land} stage !`);\n    document.removeEventListener(\"keydown\", toFight);\n    game = \"\";\n    let dungeon = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\n    game = dungeon.generate();\n    hard += 2;\n    player.level = 0;\n    land = \"dungeon\";\n  }\n  if (player.level === 30) {\n    console.log(`you have finish the game ! GG well play !!!`);\n    document.removeEventListener(\"keydown\", toFight);\n    document.removeEventListener(\"keydown\", toMove);\n    console.log(`please refresh to generate a new game !`);\n  }\n}\n\nfunction armory() {\n  document.removeEventListener(\"keydown\", toMove);\n  console.log('you have find an armory.');\n  console.log('give 50 gold to upgrade your armory (+1).');\n  console.log('tape enter to spend gold or esc to leave.');\n  document.addEventListener(\"keydown\", forge);\n}\n\nfunction swordMaster() {\n  document.removeEventListener(\"keydown\", toMove);\n  console.log('you have find a sword master.');\n  console.log('give 100 gold to train, gain 5 in force.');\n  console.log('tape enter to spend gold or esc to leave.');\n  document.addEventListener(\"keydown\", sword);\n}\n\nfunction sword(event) {\n  if (event.keyCode === 13) {\n    if (player.gold >= 100) {\n      player.gold -= 100;\n      player.force += 5;\n      console.log('you gain 5 force.');\n    } else {\n      console.log('you have not enought gold.');\n    }\n  }\n  if (event.keyCode === 27) {\n    console.log('you leave the sword master');\n    document.removeEventListener(\"keydown\", sword);\n    document.addEventListener('keydown', toMove);\n  }\n};\n\nfunction forge(event) {\n  if (event.keyCode === 13) {\n    if (player.gold >= 50) {\n      player.gold -= 50;\n      player.armor++;\n      console.log('you gain 1 armor.');\n    } else {\n      console.log('you have not enought gold.');\n    }\n  }\n  if (event.keyCode === 27) {\n    console.log('you leave the armory');\n    document.removeEventListener(\"keydown\", forge);\n    document.addEventListener('keydown', toMove);\n  }\n};\n\nfunction rogueMaster() {\n  document.removeEventListener(\"keydown\", toMove);\n  console.log('you have find a rogue master.');\n  console.log('give 75 gold to train, gain 5 in agility.');\n  console.log('tape enter to spend gold or esc to leave.');\n  document.addEventListener(\"keydown\", rogue);\n}\n\nfunction rogue(event) {\n  if (event.keyCode === 13) {\n    if (player.gold >= 75) {\n      player.gold -= 75;\n      player.agility += 5;\n      console.log('you gain 5 agility.');\n    } else {\n      console.log('you have not enought gold.');\n    }\n  }\n  if (event.keyCode === 27) {\n    console.log('you leave the rogue master');\n    document.removeEventListener(\"keydown\", rogue);\n    document.addEventListener('keydown', toMove);\n  }\n};\n\nfunction healer(event) {\n  if (event.keyCode === 13) {\n    if (player.gold >= 15) {\n      player.gold -= 15;\n      player.pv++;\n      console.log('you gain 1 pv.');\n    } else {\n      console.log('you have not enought gold.');\n    }\n  }\n  if (event.keyCode === 27) {\n    console.log('you leave the thaumaturge.');\n    document.removeEventListener(\"keydown\", healer);\n    document.addEventListener('keydown', toMove);\n  }\n};\n\nfunction thaumaturge() {\n  document.removeEventListener(\"keydown\", toMove);\n  console.log('you have find a thaumaturge.');\n  console.log('give 15 gold to heal yourself, gain 1 pv.');\n  console.log('tape enter to spend gold or esc to leave.');\n  document.addEventListener(\"keydown\", healer);\n}\n\nfunction moneyGeneration() {\n  return Math.floor((Math.random()) * 50);\n}\n\nfunction enygma() {\n  document.removeEventListener('keydown', toMove);\n  console.log(`You must find the answer to continue`);\n  let tab = [{\n    \"question\": \"I have 5 fingers but no bones. Who am I ?\",\n    \"answer\": \"glove\"\n  }, {\n    \"question\": \"How much is ((1425/6)+(3467×59867)-(9876+43)) ×0 ?\",\n    \"answer\": \"0\"\n  }, {\n    \"question\": \"I have a lock but no door. Who am I ?\",\n    \"answer\": \"padlock\"\n  }, {\n    \"question\": \"What is between EARTH and SKY ?\",\n    \"answer\": \"and\"\n  }, {\n    \"question\": \"Find the errror : 123456789\",\n    \"answer\": \"r\"\n  }, {\n    \"question\": \"One man has 7 daughters, each daugther has one brother. How many children does he have ?\",\n    \"answer\": \"8\"\n  }];\n\n  let person = \"\";\n  let choice = Math.floor(Math.random() * 6);\n  while (true) {\n    let question = tab[choice].question;\n    let answer = tab[choice].answer;\n    person = prompt(`Here's an enigma :\\n${question}`);\n    if (person === answer) {\n      game[player.x][player.y].randomEvent = 'empty';\n      let success = 4 + hard\n      player.xp += success;\n      console.log(`you have won ${success} xp.`)\n      document.addEventListener('keydown', toMove);\n      gainLevel();\n      break;\n    }\n  }\n}\n\nfunction treasure() {\n  let treasure = moneyGeneration();\n  player.gold += treasure;\n  console.log(`you have find ${treasure} gold.`);\n  gainLevel();\n  game[player.x][player.y].randomEvent = 'empty';\n}\n\nfunction trap() {\n  let test = Math.floor(Math.random() * 101);\n  if (test > player.agility) {\n    console.log(`you fail agility test with ${test}. You are trapped ! Loose ${hard} pv.`);\n    player.pv -= hard;\n    player.death();\n  } else {\n    console.log(`you disable a trap. you win ${hard} xp.`);\n    player.xp += hard;\n    gainLevel();\n  }\n  game[player.x][player.y].randomEvent = 'empty';\n}\n\nfunction trapppedTreasure() {\n  let test = Math.floor(Math.random() * 101);\n  if (player.agility < test) {\n    let bingo = moneyGeneration();\n    console.log(`You are trapped with ${test}/${player.agility} ! Loose ${hard} pv but you find a treasure : ${bingo} gold.`);\n    player.gold += bingo;\n    player.pv -= hard;\n    player.death();\n  } else {\n    let bingo = moneyGeneration();\n    player.gold += bingo;\n    player.xp += hard;\n    console.log(`you disable a trap with ${test}/${player.agility}.you have win ${bingo} gold and ${hard} xp.`);\n  };\n  gainLevel();\n  game[player.x][player.y].randomEvent = 'empty';\n}\n\nfunction nameMobGenerator() {\n  if (land === \"wood\") {\n    return 'goblin';\n  }\n  if (land === \"mountain\") {\n    return 'mountain savage';\n  }\n  if (land === \"dungeon\") {\n    return 'dark servitor';\n  }\n}\n\nfunction nameBossGenerator() {\n  if (land === \"wood\") {\n    return 'goblin tribal chief';\n  }\n  if (land === \"mountain\") {\n    return 'White Troll';\n  }\n  if (land === \"dungeon\") {\n    return 'Azazel demon';\n  }\n}\n\n// function drawGrid(grid) {\n//   let tab = document.createElement('table');\n//   let section = document.querySelector('#board');\n//   section.appendChild(tab);\n//   for (const ligne of grid) {\n//     let tr = document.createElement('tr');\n//     tab.appendChild(tr);\n//     console.log(tab);\n//     for (const room of ligne) {\n//       let td = document.createElement('td');\n//       if (player.x === room.x && player.y === room.y) {\n//         td.appendChild(player.toDraw());\n//       }\n//       tr.appendChild(td);\n//     }\n//   }\n// }\n\n// drawGrid(game);\n\n\nconsole.log('press any key to enter into the dungeon');\n\ndocument.addEventListener('keydown', start);\n\ndocument.addEventListener('keydown', toMove);\n\n\n//# sourceURL=webpack:///./src/js/game.js?");

/***/ }),

/***/ "./src/js/level.js":
/*!*************************!*\
  !*** ./src/js/level.js ***!
  \*************************/
/*! exports provided: Level */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Level\", function() { return Level; });\n/* harmony import */ var _room__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./room */ \"./src/js/room.js\");\n\n\n\"use strict\";\n\nclass Level {\n  constructor(level) {\n    this.level = level;\n  }\n  generate() {\n    let grid = [];\n    for (let x = 0; x < this.level; x++) {\n      grid[x] = [];\n      for (let y = 0; y < this.level; y++) {\n        let room = grid[x][y];\n        room = new _room__WEBPACK_IMPORTED_MODULE_0__[\"Room\"](x, y);\n        room.randomEvent = room.eventGeneration();\n        grid[x].push(room);\n      };\n    }\n    return grid;\n  }\n}\n\n//# sourceURL=webpack:///./src/js/level.js?");

/***/ }),

/***/ "./src/js/mob.js":
/*!***********************!*\
  !*** ./src/js/mob.js ***!
  \***********************/
/*! exports provided: Mob */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Mob\", function() { return Mob; });\n\n\nclass Mob {\n  constructor(name, pv, force, init, dg,armor) {\n\n    this.name = name;\n    this.pv = pv;\n    this.force = force ;\n    this.init = init;\n    this.dg = dg;\n    this.armor = armor;\n    this.boss = false;\n  }\n\n  mobDeath() {\n    if (this.pv <= 0) {\n      console.log(`you have killed this ${this.name} !`);\n      \n    }\n  }\n}\n\n\n//# sourceURL=webpack:///./src/js/mob.js?");

/***/ }),

/***/ "./src/js/player.js":
/*!**************************!*\
  !*** ./src/js/player.js ***!
  \**************************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Player\", function() { return Player; });\n\n\n\nclass Player {\n  constructor(name, x, y, pv, force, init, armor, power, xp, level = 0, gold, inventory = [], agility, localisation) {\n\n    this.name = name;\n    this.x = x;\n    this.y = y;\n    this.pv = pv;\n    this.force = force;\n    this.agility = agility;\n    this.init = init;\n    this.power = power;\n    this.armor = armor;\n    this.xp = xp;\n    this.level = level;\n    this.gold = gold;\n    this.inventory = inventory;\n    this.localisation = localisation - 1;\n  }\n  moveUp() {\n    this.x--;\n    this.toDraw();\n    if (this.x < 0) {\n      console.log('you cannot move to this direction.');\n      this.x = 0;\n    }\n  }\n  moveDown() {\n    this.x++;\n    this.toDraw();\n    if (this.x >= this.localisation) {\n      console.log('you cannot move to this direction.');\n      this.x = this.localisation;\n    }\n  }\n  moveLeft() {\n    this.y--;\n    this.toDraw();\n    if (this.y < 0) {\n      console.log('you cannot move to this direction.');\n      this.y = 0;\n    }\n  }\n  moveRight() {\n    this.y++;\n    this.toDraw();\n    if (this.y >= this.localisation) {\n      console.log('you cannot move to this direction.');\n      this.y = this.localisation;\n    }\n  }\n  totalArmor() {\n    return this.armor;\n  }\n  totalAttack() {\n    return this.power;\n  }\n  death() {\n    if (this.pv <= 0) {\n      console.log(`you haved been killed !`);\n      player = \"\";\n      document.removeEventListener(\"keydown\", toFight);\n    }\n  }\n  toDraw() {\n    // board.innerHTML = \"\";\n    // let playerVisual = document.createElement(\"div\");\n    // playerVisual.style.position = \"absolute\";\n    // playerVisual.style.height = `${25}px`;\n    // playerVisual.style.width = `${25}px`;\n    // playerVisual.style.backgroundColor = \"black\";\n    // playerVisual.setAttribute(\"id\", \"me\");\n    // board.appendChild(playerVisual);\n    // playerVisual.style.top = `${this.x * 50}px`;\n    // playerVisual.style.left = `${this.y * 50}px`;\n  }\n}\n\n\n//# sourceURL=webpack:///./src/js/player.js?");

/***/ }),

/***/ "./src/js/room.js":
/*!************************!*\
  !*** ./src/js/room.js ***!
  \************************/
/*! exports provided: Room */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Room\", function() { return Room; });\n\n\nclass Room {\n  constructor(x, y, ground = [], randomEvent) {\n    this.x = x;\n    this.y = y;\n    this.ground = ground;\n    this.randomEvent = randomEvent;\n  }\n\n  trader() {\n    let i = ['armory', 'sword master', 'rogue master', 'thaumaturge'];\n    return i[Math.floor(Math.random() * i.length)]\n  }\n\n  eventGeneration() {\n    let tab = ['empty', 'ennemy', 'enygma', 'treasure', 'ennemy', 'trap', 'trapped-treasure', 'ennemy', 'boss', 'trader'];\n    let random = tab[Math.floor(Math.random() * tab.length)];\n    if (random === 'trader') {\n      let trade = this.trader()\n      if (trade === 'armory') {\n        random = 'armory';\n      } else if (trade === 'sword master') {\n        random = 'sword master';\n      } else if (trade === 'rogue master') {\n        random = 'rogue master';\n      } else if (trade === 'thaumaturge') {\n        random = 'thaumaturge';\n      }\n    }\n    return random;\n  }\n}\n\n\n\n//# sourceURL=webpack:///./src/js/room.js?");

/***/ })

/******/ });