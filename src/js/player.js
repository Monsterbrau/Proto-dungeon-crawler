
"use strict";

export class Player {
  constructor(name, x, y, pv, force, init, armor, power, xp, level = 0, gold, inventory = [], agility, localisation) {

    this.name = name;
    this.x = x;
    this.y = y;
    this.pv = pv;
    this.force = force;
    this.agility = agility;
    this.init = init;
    this.power = power;
    this.armor = armor;
    this.xp = xp;
    this.level = level;
    this.gold = gold;
    this.inventory = inventory;
    this.localisation = localisation - 1;
  }
  moveUp() {
    this.x--;
    this.toDraw();
    if (this.x < 0) {
      console.log('you cannot move to this direction.');
      this.x = 0;
    }
  }
  moveDown() {
    this.x++;
    this.toDraw();
    if (this.x >= this.localisation) {
      console.log('you cannot move to this direction.');
      this.x = this.localisation;
    }
  }
  moveLeft() {
    this.y--;
    this.toDraw();
    if (this.y < 0) {
      console.log('you cannot move to this direction.');
      this.y = 0;
    }
  }
  moveRight() {
    this.y++;
    this.toDraw();
    if (this.y >= this.localisation) {
      console.log('you cannot move to this direction.');
      this.y = this.localisation;
    }
  }
  totalArmor() {
    return this.armor;
  }
  totalAttack() {
    return this.power;
  }
  death() {
    if (this.pv <= 0) {
      console.log(`you haved been killed !`);
      player = "";
      document.removeEventListener("keydown", toFight);
    }
  }
  toDraw() {
    board.innerHTML = "";
    let playerVisual = document.createElement("div");
    // playerVisual.style.position = "absolute";
    // playerVisual.style.height = `${25}px`;
    // playerVisual.style.width = `${25}px`;
    // playerVisual.style.backgroundColor = "black";
    playerVisual.setAttribute("id", "me");
    board.appendChild(playerVisual);
    playerVisual.style.top = `${this.x * 50}px`;
    playerVisual.style.left = `${this.y * 50}px`;
  }
}
