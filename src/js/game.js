"use strict";

import { Player } from './player';
import { Mob } from './mob';
import { Level } from './level'

let player;
let game = [];
let ennemy = new Mob();


let stage = 5;
let forest = new Level(stage);
let hard = 1;
let land = "";

function start() {
  console.log("to finish the stage : gain 10 level !");
  document.removeEventListener('keydown', start);
  land = "wood"
  game = forest.generate();
  player = new Player("Monsterbrau", 0, 0, 7, 65, 2, 5, 5, 0, 0, 0, [], 55, stage);
  game[player.x][player.y];
  console.log(game);
  player.toDraw();
  console.log("use z,q,s,d, keys in your keyboard to move into the dungeon");
}

function toMove(event) {
  gainLevel();
  if (event.keyCode === 90) {
    player.moveUp();
    // board.innerHTML = "";
    // drawGrid(game);
  } else if (event.keyCode === 83) {
    player.moveDown();
    // board.innerHTML = "";
    // drawGrid(game);
  } else if (event.keyCode === 81) {
    player.moveLeft();
    // board.innerHTML = "";
    // drawGrid(game);
  } else if (event.keyCode === 68) {
    player.moveRight();
    // board.innerHTML = "";
    // drawGrid(game);
  }

  console.log(game[player.x][player.y]);

  if ((game[player.x][player.y].randomEvent) === 'treasure') {
    treasure();
  }

  if (game[player.x][player.y].randomEvent === "ennemy") {
    console.log(`you have encounter an ennemy !`);
    document.removeEventListener('keydown', toMove);
    console.log('press enter to begin the fight');
    document.addEventListener('keydown', toFight);
  }

  if (game[player.x][player.y].randomEvent === "enygma") {
    enygma();
  }

  if (game[player.x][player.y].randomEvent === "trap") {
    trap();
  }

  if (game[player.x][player.y].randomEvent === 'trapped-treasure') {
    trapppedTreasure();
  }

  if (game[player.x][player.y].randomEvent === 'boss') {
    document.removeEventListener('keydown', toMove);
    console.log('press enter to begin the fight');
    document.addEventListener('keydown', toFight);
  }

  if (game[player.x][player.y].randomEvent === 'armory') {
    armory();
  }
  if (game[player.x][player.y].randomEvent === 'sword master') {
    swordMaster();
  }
  if (game[player.x][player.y].randomEvent === 'rogue master') {
    rogueMaster();
  }
  if (game[player.x][player.y].randomEvent === 'thaumaturge') {
    thaumaturge();
  }
}

function mobGeneration() {
  let mobName = nameMobGenerator();
  let percent = Math.floor(Math.random() * hard + 45);
  let num = Math.floor(Math.random() * hard * 2);
  if (num === 0) {
    num += hard;
  }
  let mob = new Mob(mobName, num, percent, num, num, num);
  return mob;
}

function bossGeneration() {
  let mobName = nameBossGenerator(land);
  let percent = Math.floor(Math.random() * hard + 65);
  let num = Math.floor(Math.random() * hard * 5);
  if (num === 0) {
    num += 5;
  }
  let boss = new Mob(mobName, num, percent, num, num, num);
  boss.boss = true;
  return boss;
}

function firstStrike() {
  console.log(`you have first strike !`);
  let dd = 100;
  let attack = Math.floor(Math.random() * dd);
  if (attack <= player.force) {
    console.log(`attack success with ${attack} !`);
    let hit = player.totalAttack() - ennemy.armor;
    if (hit <= 0) {
      hit = 1
    }
    ennemy.pv -= hit;
    dd -= 10;
    console.log(`you have made ${hit} dommages !`);
    ennemy.mobDeath();
  } else {
    console.log('attack fail !');
    let dd = 100;
    let attack = Math.floor(Math.random() * dd);
    if (attack <= ennemy.force) {
      console.log(`${ennemy.name} attack success with ${attack} !`);
      let hit = ennemy.dg - player.totalArmor();
      if (hit <= 0) {
        hit = 1
      }
      player.pv -= hit;
      dd -= 10
      console.log(`you have take ${hit} dommages !`);
      player.death();
    } else {
      console.log(`${ennemy.name}'s attack fail !`);
    }
  };

}

function secondStrike() {
  console.log(`${ennemy.name} first strike !`);
  let dd = 100;
  let attack = Math.floor(Math.random() * dd);
  if (attack <= ennemy.force) {
    console.log(`${ennemy.name} attack success with ${attack} !`);
    let hit = ennemy.dg - player.totalArmor();
    if (hit <= 0) {
      hit = 1
    }
    player.pv -= hit;
    dd -= 10;
    console.log(`you have take ${hit} dommages !`);
    player.death();
  } else {
    console.log(`${ennemy.name}'s attack fail !`);
    let dd = 100;
    let attack = Math.floor(Math.random() * dd);
    if (attack <= player.force) {
      console.log(`attack success with ${attack} !`);
      let hit = player.totalAttack() - ennemy.armor;
      if (hit <= 0) {
        hit = 1
      }
      ennemy.pv -= hit;
      dd -= 10;
      console.log(`you have made ${hit} dommages !`);
      ennemy.mobDeath();
    } else {
      console.log('attack fail !');
    }
  }
}

function toFight(event) {
  if (event.keyCode === 13) {
    if (game[player.x][player.y].randomEvent === 'boss') {
      ennemy = bossGeneration();
    } else { ennemy = mobGeneration(hard); };
    console.log(ennemy);
    while (player.pv > 0 && ennemy.pv > 0) {
      if (player.init >= ennemy.init) {
        firstStrike();
      } else {
        secondStrike();
      }
    }
    if (ennemy.boss) {
      hard++;
      player.gold += moneyGeneration();
      player.level++;
    }
    player.xp += (hard + 3);
    console.log(`player xp : ${player.xp}`);
    player.gold += moneyGeneration();
    gainLevel();
    console.log(`player pv : ${player.pv}`);
    console.log(`player gold : ${player.gold}`);
    console.log(`player level : ${player.level}`);
    console.log(`difficulty : ${hard}`);

    game[player.x][player.y].randomEvent = 'empty';
    document.removeEventListener("keydown", toFight);
    document.addEventListener('keydown', toMove);
  }
}

function gainLevel() {

  if (player.xp >= 5) {
    player.xp -= 5;
    player.level++;
    player.force += 2;
    player.init++;
    player.pv += hard;
    console.log(`you are level ${player.level}.`);
    console.log(player);
  }
  if (player.level === 10) {
    console.log(`you have win the ${land} stage !`);
    document.removeEventListener("keydown", toFight);
    game = "";
    let mountain = new Level(stage);
    game = mountain.generate();
    hard += 2;
    player.level = 0;
    player.x = 0;
    player.y = 0;
    land = "mountain";
  }
  if (player.level === 20) {
    console.log(`you have win the ${land} stage !`);
    document.removeEventListener("keydown", toFight);
    game = "";
    let dungeon = new Level(stage);
    game = dungeon.generate();
    hard += 2;
    player.level = 0;
    land = "dungeon";
  }
  if (player.level === 30) {
    console.log(`you have finish the game ! GG well play !!!`);
    document.removeEventListener("keydown", toFight);
    document.removeEventListener("keydown", toMove);
    console.log(`please refresh to generate a new game !`);
  }
}

function armory() {
  document.removeEventListener("keydown", toMove);
  console.log('you have find an armory.');
  console.log('give 50 gold to upgrade your armory (+1).');
  console.log('tape enter to spend gold or esc to leave.');
  document.addEventListener("keydown", forge);
}

function swordMaster() {
  document.removeEventListener("keydown", toMove);
  console.log('you have find a sword master.');
  console.log('give 100 gold to train, gain 5 in force.');
  console.log('tape enter to spend gold or esc to leave.');
  document.addEventListener("keydown", sword);
}

function sword(event) {
  if (event.keyCode === 13) {
    if (player.gold >= 100) {
      player.gold -= 100;
      player.force += 5;
      console.log('you gain 5 force.');
    } else {
      console.log('you have not enought gold.');
    }
  }
  if (event.keyCode === 27) {
    console.log('you leave the sword master');
    document.removeEventListener("keydown", sword);
    document.addEventListener('keydown', toMove);
  }
};

function forge(event) {
  if (event.keyCode === 13) {
    if (player.gold >= 50) {
      player.gold -= 50;
      player.armor++;
      console.log('you gain 1 armor.');
    } else {
      console.log('you have not enought gold.');
    }
  }
  if (event.keyCode === 27) {
    console.log('you leave the armory');
    document.removeEventListener("keydown", forge);
    document.addEventListener('keydown', toMove);
  }
};

function rogueMaster() {
  document.removeEventListener("keydown", toMove);
  console.log('you have find a rogue master.');
  console.log('give 75 gold to train, gain 5 in agility.');
  console.log('tape enter to spend gold or esc to leave.');
  document.addEventListener("keydown", rogue);
}

function rogue(event) {
  if (event.keyCode === 13) {
    if (player.gold >= 75) {
      player.gold -= 75;
      player.agility += 5;
      console.log('you gain 5 agility.');
    } else {
      console.log('you have not enought gold.');
    }
  }
  if (event.keyCode === 27) {
    console.log('you leave the rogue master');
    document.removeEventListener("keydown", rogue);
    document.addEventListener('keydown', toMove);
  }
};

function healer(event) {
  if (event.keyCode === 13) {
    if (player.gold >= 15) {
      player.gold -= 15;
      player.pv++;
      console.log('you gain 1 pv.');
    } else {
      console.log('you have not enought gold.');
    }
  }
  if (event.keyCode === 27) {
    console.log('you leave the thaumaturge.');
    document.removeEventListener("keydown", healer);
    document.addEventListener('keydown', toMove);
  }
};

function thaumaturge() {
  document.removeEventListener("keydown", toMove);
  console.log('you have find a thaumaturge.');
  console.log('give 15 gold to heal yourself, gain 1 pv.');
  console.log('tape enter to spend gold or esc to leave.');
  document.addEventListener("keydown", healer);
}

function moneyGeneration() {
  return Math.floor((Math.random()) * 50);
}

function enygma() {
  document.removeEventListener('keydown', toMove);
  console.log(`You must find the answer to continue`);
  let tab = [{
    "question": "I have 5 fingers but no bones. Who am I ?",
    "answer": "glove"
  }, {
    "question": "How much is ((1425/6)+(3467×59867)-(9876+43)) ×0 ?",
    "answer": "0"
  }, {
    "question": "I have a lock but no door. Who am I ?",
    "answer": "padlock"
  }, {
    "question": "What is between EARTH and SKY ?",
    "answer": "and"
  }, {
    "question": "Find the errror : 123456789",
    "answer": "r"
  }, {
    "question": "One man has 7 daughters, each daugther has one brother. How many children does he have ?",
    "answer": "8"
  }];

  let person = "";
  let choice = Math.floor(Math.random() * 6);
  while (true) {
    let question = tab[choice].question;
    let answer = tab[choice].answer;
    person = prompt(`Here's an enigma :\n${question}`);
    if (person === answer) {
      game[player.x][player.y].randomEvent = 'empty';
      let success = 4 + hard
      player.xp += success;
      console.log(`you have won ${success} xp.`)
      document.addEventListener('keydown', toMove);
      gainLevel();
      break;
    }
  }
}

function treasure() {
  let treasure = moneyGeneration();
  player.gold += treasure;
  console.log(`you have find ${treasure} gold.`);
  gainLevel();
  game[player.x][player.y].randomEvent = 'empty';
}

function trap() {
  let test = Math.floor(Math.random() * 101);
  if (test > player.agility) {
    console.log(`you fail agility test with ${test}. You are trapped ! Loose ${hard} pv.`);
    player.pv -= hard;
    player.death();
  } else {
    console.log(`you disable a trap. you win ${hard} xp.`);
    player.xp += hard;
    gainLevel();
  }
  game[player.x][player.y].randomEvent = 'empty';
}

function trapppedTreasure() {
  let test = Math.floor(Math.random() * 101);
  if (player.agility < test) {
    let bingo = moneyGeneration();
    console.log(`You are trapped with ${test}/${player.agility} ! Loose ${hard} pv but you find a treasure : ${bingo} gold.`);
    player.gold += bingo;
    player.pv -= hard;
    player.death();
  } else {
    let bingo = moneyGeneration();
    player.gold += bingo;
    player.xp += hard;
    console.log(`you disable a trap with ${test}/${player.agility}.you have win ${bingo} gold and ${hard} xp.`);
  };
  gainLevel();
  game[player.x][player.y].randomEvent = 'empty';
}

function nameMobGenerator() {
  if (land === "wood") {
    return 'goblin';
  }
  if (land === "mountain") {
    return 'mountain savage';
  }
  if (land === "dungeon") {
    return 'dark servitor';
  }
}

function nameBossGenerator() {
  if (land === "wood") {
    return 'goblin tribal chief';
  }
  if (land === "mountain") {
    return 'White Troll';
  }
  if (land === "dungeon") {
    return 'Azazel demon';
  }
}

function drawGrid(grid) {
  let tab = document.createElement('table');
  let section = document.querySelector('#board');
  section.appendChild(tab);
  for (const ligne of grid) {
    let tr = document.createElement('tr');
    tab.appendChild(tr);
    console.log(tab);
    for (const room of ligne) {
      let td = document.createElement('td');
      if (player.x === room.x && player.y === room.y) {
        td.appendChild(player.toDraw());
      }
      tr.appendChild(td);
    }
  }
}

drawGrid(game);


console.log('press any key to enter into the dungeon');

document.addEventListener('keydown', start);

document.addEventListener('keydown', toMove);
